/** Caribou bare device example implementation
 */

#include "AD9249Device.hpp"
#include "utils/log.hpp"

using namespace caribou;

AD9249Device::AD9249Device(caribou::Configuration config)
    : CaribouDevice(config, InterfaceConfiguration(config.Get("devicepath", std::string(DEFAULT_DEVICEPATH)))) {

  _dispatcher.add("frobicate", &AD9249Device::frobicate, this);
  _dispatcher.add("unfrobicate", &AD9249Device::unfrobicate, this);
}

AD9249Device::~AD9249Device() {
  LOG(INFO) << "Shutdown, delete device";
}

std::string AD9249Device::getFirmwareVersion() {
  return "42.23alpha";
}

std::string AD9249Device::getType() {
  return "AD9249";
}

void AD9249Device::powerOn() {
  LOG(INFO) << "Power on";
}

void AD9249Device::powerOff() {
  LOG(INFO) << "Power off";
}

void AD9249Device::powerUp() {
  LOG(INFO) << "Power on";
}

void AD9249Device::powerDown() {
  LOG(INFO) << "Power off";
}

void AD9249Device::daqStart() {
  LOG(INFO) << "DAQ started";
}

void AD9249Device::daqStop() {
  LOG(INFO) << "DAQ stopped";
}

pearydata AD9249Device::getData() {
  pearydata x;
  x[{0u, 0u}] = std::make_unique<pixel>();
  x[{8u, 16u}] = std::make_unique<pixel>();
  return x;
}

// custom device functionality exported via the dispatcher

int32_t AD9249Device::frobicate(int32_t a) {
  return (a << 2) & 0b010101010101010101010101010101;
}

std::string AD9249Device::unfrobicate(int32_t b) {
  return "blub" + std::to_string(b);
}
