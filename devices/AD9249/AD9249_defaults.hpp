#ifndef DEVICE_CLICPIX2_DEFAULTS_H
#define DEVICE_CLICPIX2_DEFAULTS_H

#include "utils/dictionary.hpp"

namespace caribou {

/** Default device path for this device: SPI interface
 */
#define DEFAULT_DEVICEPATH "/dev/spi"

  // AD9249 IP Control
  const std::intptr_t AD9249_RECEIVER_BASE_ADDRESS = 0x43C00000;
  const std::size_t AD9249_RECEIVER_MAP_SIZE = 4096;

  // ADC0 readout
  const std::intptr_t ADC0_READOUT_BASE_ADDRESS = 0x00000000;
  const std::size_t ADC0_READOUT_MAP_SIZE = 134217728;

  // ADC1 readout
  const std::intptr_t ADC1_READOUT_BASE_ADDRESS = 0x08000000;
  const std::size_t ADC1_READOUT_MAP_SIZE = 134217728;

#define AD9249_MEMORY                                                                                                       \
  {                                                                                                                         \
    {"data_gen_reset", memory_map(AD9249_RECEIVER_BASE_ADDRESS, 0x0, AD9249_RECEIVER_MAP_SIZE, PROT_READ | PROT_WRITE)},    \
      {"trigger", memory_map(AD9249_RECEIVER_BASE_ADDRESS, 0x4, AD9249_RECEIVER_MAP_SIZE, PROT_READ | PROT_WRITE)},         \
      {"fifo_reset", memory_map(AD9249_RECEIVER_BASE_ADDRESS, 0x8, AD9249_RECEIVER_MAP_SIZE, PROT_READ | PROT_WRITE)},      \
      {"address_reset", memory_map(AD9249_RECEIVER_BASE_ADDRESS, 0xC, AD9249_RECEIVER_MAP_SIZE, PROT_READ | PROT_WRITE)},   \
      {"burst_enable", memory_map(AD9249_RECEIVER_BASE_ADDRESS, 0x10, AD9249_RECEIVER_MAP_SIZE, PROT_READ | PROT_WRITE)},   \
      {"test_data_enable",                                                                                                  \
       memory_map(AD9249_RECEIVER_BASE_ADDRESS, 0x14, AD9249_RECEIVER_MAP_SIZE, PROT_READ | PROT_WRITE)},                   \
      {"adc0_burst_length",                                                                                                 \
       memory_map(AD9249_RECEIVER_BASE_ADDRESS, 0x18, AD9249_RECEIVER_MAP_SIZE, PROT_READ | PROT_WRITE)},                   \
      {"adc0_current_address",                                                                                              \
       memory_map(AD9249_RECEIVER_BASE_ADDRESS, 0x1C, AD9249_RECEIVER_MAP_SIZE, PROT_READ | PROT_WRITE)},                   \
      {"adc1_burst_length",                                                                                                 \
       memory_map(AD9249_RECEIVER_BASE_ADDRESS, 0x20, AD9249_RECEIVER_MAP_SIZE, PROT_READ | PROT_WRITE)},                   \
      {"adc1_current_address",                                                                                              \
       memory_map(AD9249_RECEIVER_BASE_ADDRESS, 0x24, AD9249_RECEIVER_MAP_SIZE, PROT_READ | PROT_WRITE)},                   \
      {"adc_data_clk_freq",                                                                                                 \
       memory_map(AD9249_RECEIVER_BASE_ADDRESS, 0x28, AD9249_RECEIVER_MAP_SIZE, PROT_READ | PROT_WRITE)},                   \
      {"adc_frame_clk_freq",                                                                                                \
       memory_map(AD9249_RECEIVER_BASE_ADDRESS, 0x2C, AD9249_RECEIVER_MAP_SIZE, PROT_READ | PROT_WRITE)},                   \
      {"adc_framedata_alignment",                                                                                           \
       memory_map(AD9249_RECEIVER_BASE_ADDRESS, 0x30, AD9249_RECEIVER_MAP_SIZE, PROT_READ | PROT_WRITE)},                   \
      {"adc_finedelay", memory_map(AD9249_RECEIVER_BASE_ADDRESS, 0x34, AD9249_RECEIVER_MAP_SIZE, PROT_READ | PROT_WRITE)},  \
      {"adc_fine_delay_rb",                                                                                                 \
       memory_map(AD9249_RECEIVER_BASE_ADDRESS, 0x38, AD9249_RECEIVER_MAP_SIZE, PROT_READ | PROT_WRITE)},                   \
      {"fine_delay_stable",                                                                                                 \
       memory_map(AD9249_RECEIVER_BASE_ADDRESS, 0x3C, AD9249_RECEIVER_MAP_SIZE, PROT_READ | PROT_WRITE)},                   \
      {"ADC0_MEMORY", memory_map(ADC0_READOUT_BASE_ADDRESS, 0x0, ADC0_READOUT_MAP_SIZE, PROT_READ | PROT_WRITE)},           \
      {"ADC1_MEMORY", memory_map(ADC1_READOUT_BASE_ADDRESS, 0x0, ADC1_READOUT_MAP_SIZE, PROT_READ | PROT_WRITE)},           \
  }

// clang-format off
#define AD9249_REGISTERS						\
  {									\
    {"spi_port_config", register_t<>(0x00, 0xFF,0x18, true, true,false)},			\
    {"chip_id", register_t<>(0x01, 0xFF,0x92, true, false,false)},	\
    {"chip_grade", register_t<>(0x02,0xFF,0b00110000,false,true,false)},				\
    {"device_index2", register_t<>(0x04,0xFF,0x0F,true,true,false)},				\
    {"device_index1", register_t<>(0x05,0xFF,0x0F,true,true,false)},					\
    {"transfer", register_t<>(0xFF,0xFF,0x00,true,true,false)},				\
    {"power_mode", register_t<>(0x8,0xFF,0x0,true,true,false)},				\
    {"clock", register_t<>(0x09,0xFF, 0x01,true,true,false)},			\
    {"clock_divide", register_t<>(0x0B, 0xFF,0x0,true,true,false)},			\
    {"enhancement_control", register_t<>(0x0C,0xFF,0x0,true,true,false)},				\
    {"test_mode", register_t<>(0x0x0D,0xFF,0x0,true,true,false)},				\
    {"offset_adjust", register_t<>(0x10,0xFF,0x00,true,true,false)},				\
    {"output_mode", register_t<>(0x14,0xFF,0x01,true,true,false)},					\
    {"output_adjust", register_t<>(0x15,0xFF,0x0,true,true,false)},				\
    {"output_phase", register_t<>(0x16,0xFF,0x03,true,true,false)},				\
    {"vref", register_t<>(0x16,0xFF,0x03,true,true,false)},				\
    {"user_pattern1_lsb", register_t<>(0x19,0xFF,0x0,true,true,false)},				\
    {"user_pattern1_msb", register_t<>(0x1A,0xFF,0x0,true,true,false)},				\
    {"user_pattern2_lsb", register_t<>(0x1B, 0xFF,0x0, false, true, true)},  	\
    {"user_pattern2_msb", register_t<>(0x1C,0xFF,0x0,true,true,false)},				\
    {"serial_output_data_ctrl", register_t<>(0x21,0xFF,0x41,true,true,false)},				\
    {"serial_channel_status", register_t<>(0x22, 0xFF,0x0, true, true, false)},          \
    {"resolution_sample_override", register_t<>(0x100,0xFF,0x00,true,true,false)},  				\
    {"user_io_ctrl2", register_t<>(0x101,0xFF,0x0,true,true,false)},				    									\
    {"user_io_ctrl3", register_t<>(0x102, 0xFF,0x0,true,true,false)},			\
    {"sync", register_t<>(0x109,0xFF,0x0,true,true,false)},				\
  }
  // clang-format on

} // namespace caribou

#endif /* DEVICE_AD9249_DEFAULTS_H */
