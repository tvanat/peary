/**
 * Caribou Dso9254a Device implementation
 */

#include "DSO9254ADevice.hpp"
#include <fstream>
#include <sstream>
#include <string>
#include "utils/log.hpp"

using namespace caribou;

DSO9254ADevice::DSO9254ADevice(const caribou::Configuration config)
    : AuxiliaryDevice(config, InterfaceConfiguration(config.Get("devicepath", std::string(DEFAULT_DEVICEPATH)))) {

  // FIXME dispatcher functions
  _dispatcher.add("query", &DSO9254ADevice::query, this);
  _dispatcher.add("send", &DSO9254ADevice::send, this);
  _dispatcher.add("waitForTrigger", &DSO9254ADevice::waitForTrigger, this);
  _dispatcher.add("getBinaryData", &DSO9254ADevice::getBinaryData, this);

  LOG(DEBUG) << "New scope interface, trying to connect...";
  std::string query = "*IDN?";
  std::string data = AuxiliaryDevice<iface_ipsocket>::receive(query).front();

  LOG(DEBUG) << "Connected successfully to "
             << InterfaceManager::getInterface<iface_ipsocket>(_interfaceConfig).devicePath();
  LOG(DEBUG) << "Scope identifier: " << data;
}

DSO9254ADevice::~DSO9254ADevice() {
  LOG(INFO) << "Shutdown, delete device.";
}

void DSO9254ADevice::send(std::string command) {
  LOG(DEBUG) << "Command: " << command;
  AuxiliaryDevice<iface_ipsocket>::send(command);
}

std::string DSO9254ADevice::query(const std::string query) {
  std::string s = AuxiliaryDevice<iface_ipsocket>::receive(query).front();
  LOG(DEBUG) << s;
  return s;
}

void DSO9254ADevice::configure() {
  std::string path = _config.Get("config", "");

  if(path.empty()) {
    return;
  }

  std::ifstream file(path);
  std::string line;

  while(std::getline(file, line)) {
    if(!line.empty() && line[0] != '#') {
      send(line);
    }
  }
}

int DSO9254ADevice::waitForTrigger() {

  std::string query = ":ter?";

  int trigger = 0;
  while(trigger == 0) {
    mDelay(10);
    trigger = std::stoi(AuxiliaryDevice<iface_ipsocket>::receive(query).front());
  }

  return trigger;
}

pearydata DSO9254ADevice::getData() {

  std::vector<double> values;

  LOG(INFO) << "Retrieving data from scope...";
  std::string cmd = "wav:data?";
  auto data = AuxiliaryDevice<iface_ipsocket>::receive(cmd);

  LOG(INFO) << "Crunching numbers...";

  if(data.empty()) {
    return pearydata();
    // return std::vector<double>();
  }

  std::istringstream ss(data.front());
  std::string token;

  while(std::getline(ss, token, ',')) {
    values.push_back(std::stod(token));
  }
  // return values;
  return pearydata();
}

pearydataVector DSO9254ADevice::getData(const unsigned int) {
  LOG(FATAL) << "Data readback not implemented for this device";
  throw caribou::DeviceImplException("Data readback not implemented for this device");
}

void DSO9254ADevice::getBinaryData(std::string out) {
  auto p = AuxiliaryDevice<iface_ipsocket>::receive(":WAVeform:PREamble?");

  std::ofstream file(out + ".txt");
  file << p.front();

  LOG(DEBUG) << "Retrieving data from scope...";
  LOG(DEBUG) << "Preamble: " << p.front();

  std::istringstream s(p.front());
  std::string str;
  std::vector<std::string> preamble;

  while(std::getline(s, str, ',')) {
    preamble.push_back(str);
  }

  size_t points = std::stoul(preamble[2]);
  size_t segments = preamble.size() == 25 ? std::stoul(preamble[24]) : 1;

  size_t size = 2;

  //+3 bytes for streaming header and end character
  auto data = InterfaceManager::getInterface<iface_ipsocket>(_interfaceConfig)
                .read_binary(":WAVEFORM:DATA?", static_cast<unsigned int>(points * size * segments + 3));

  std::ofstream file2(out + ".dat", std::ofstream::binary);
  file2.write(reinterpret_cast<char*>(&data[2]), static_cast<long int>(points * size * segments));
}

void DSO9254ADevice::daqStart(void) {
  // ensure only one daq thread is running
  if(daqRunning) {
    LOG(WARNING) << "Data aquisition is already running";
    return;
  }

  if(_daqThread.joinable()) {
    LOG(WARNING) << "DAQ thread is already running";
    return;
  }

  _daqContinue.test_and_set();
  _daqThread = std::thread(&DSO9254ADevice::runDaq, this);

  daqRunning = true;
}

void DSO9254ADevice::daqStop(void) {
  if(_daqThread.joinable()) {
    // signal to daq thread that we want to stop and wait until it does
    _daqContinue.clear();
    _daqThread.join();
  }
  _daqContinue.clear();
  daqRunning = false;
}

void DSO9254ADevice::runDaq(void) {
  size_t count = 0;
  std::vector<int> channels;
  bool flag;

  for(int i = 1; i <= 4; i++) {
    std::string cmd = ":CHANNEL" + std::to_string(i) + ":DISPLAY?";
    if(std::stoi(AuxiliaryDevice<iface_ipsocket>::receive(cmd).front()) == 1) {
      channels.push_back(i);
    }
  }

  AuxiliaryDevice<iface_ipsocket>::receive(":ADER?"); // clear flag

  for(;;) {
    AuxiliaryDevice<iface_ipsocket>::send(":SINGLE");

    LOG(DEBUG) << "Count = " << count;

    while(std::stoi(AuxiliaryDevice<iface_ipsocket>::receive(":ADER?").front()) == 0) {
      flag = _daqContinue.test_and_set();
      if(!flag) {
        AuxiliaryDevice<iface_ipsocket>::send(":STOP");
        break;
      }

      mDelay(1000);
    }

    LOG(DEBUG) << "Reading block " << count;

    for(auto ch : channels) {
      std::string file = "data_" + std::to_string(count) + "_ch" + std::to_string(ch);
      std::string cmd = ":WAVeform:SOURce CHANnel" + std::to_string(ch);
      AuxiliaryDevice<iface_ipsocket>::send(cmd);
      getBinaryData(file);
    }

    if(!flag) {
      return;
    }

    count++;
  }
}
