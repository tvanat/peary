/**
 * Caribou implementation for the dSiPM
 */

#include "dSiPMDevice.hpp"
#include "utils/log.hpp"

#include <fstream>

using namespace caribou;

dSiPMDevice::dSiPMDevice(const caribou::Configuration config)
    : CaribouDevice(config, iface_mem::configuration_type(MEM_PATH, DSIPM_READOUT)) {

  _dispatcher.add("configureClock", &dSiPMDevice::configureClock, this);
  _dispatcher.add("powerStatusLog", &dSiPMDevice::powerStatusLog, this);
  _dispatcher.add("configureMatrix", &dSiPMDevice::configureMatrix, this);
  _dispatcher.add("ledOn", &dSiPMDevice::ledOn, this);
  _dispatcher.add("ledOff", &dSiPMDevice::ledOff, this);

  // Set up periphery
  _periphery.add("VDDIO3V3", carboard::PWR_OUT_1);
  _periphery.add("VDDIO1V8", carboard::PWR_OUT_2);
  _periphery.add("VDDCORE", carboard::PWR_OUT_3);
  _periphery.add("LED_PWR", carboard::PWR_OUT_4);
  _periphery.add("DS_PWR", carboard::PWR_OUT_6);

  _periphery.add("BIAS_1", carboard::BIAS_4);
  _periphery.add("BIAS_2", carboard::BIAS_2);
  _periphery.add("BIAS_3", carboard::BIAS_1);
  _periphery.add("BIAS_4", carboard::BIAS_3);

  _periphery.add("CMOS_OUT_1_TO_4", carboard::CMOS_OUT_1_TO_4);
  _periphery.add("CMOS_OUT_5_TO_8", carboard::CMOS_OUT_5_TO_8);

  _periphery.add("TD_IN", carboard::CUR_1);
  _periphery.add("TD_OUT", carboard::VOL_IN_1);

  // Add the register definitions to the dictionary for convenient lookup of names:
  _registers.add(DSIPM_REGISTERS);

  // Add memory pages to the dictionary:
  _memory.add(DSIPM_MEMORY);

  // Matrix not configured yet:
  matrixConfigured_ = false;
}

void dSiPMDevice::powerStatusLog() {
  LOG(INFO) << "Power status:";

  LOG(INFO) << "VDDIO3V3:";
  LOG(INFO) << "\tBus voltage: " << this->getVoltage("VDDIO3V3") << "V";
  LOG(INFO) << "\tBus current: " << this->getCurrent("VDDIO3V3") << "A";
  LOG(INFO) << "\tBus power  : " << this->getPower("VDDIO3V3") << "W";

  LOG(INFO) << "VDDIO1V8:";
  LOG(INFO) << "\tBus voltage: " << this->getVoltage("VDDIO1V8") << "V";
  LOG(INFO) << "\tBus current: " << this->getCurrent("VDDIO1V8") << "A";
  LOG(INFO) << "\tBus power  : " << this->getPower("VDDIO1V8") << "W";

  LOG(INFO) << "VDDCORE:";
  LOG(INFO) << "\tBus voltage: " << this->getVoltage("VDDCORE") << "V";
  LOG(INFO) << "\tBus current: " << this->getCurrent("VDDCORE") << "A";
  LOG(INFO) << "\tBus power  : " << this->getPower("VDDCORE") << "W";

  LOG(INFO) << "LED_PWR:";
  LOG(INFO) << "\tBus voltage: " << this->getVoltage("LED_PWR") << "V";
  LOG(INFO) << "\tBus current: " << this->getCurrent("LED_PWR") << "A";
  LOG(INFO) << "\tBus power  : " << this->getPower("LED_PWR") << "W";

  LOG(INFO) << "DS_PWR:";
  LOG(INFO) << "\tBus voltage: " << this->getVoltage("DS_PWR") << "V";
  LOG(INFO) << "\tBus current: " << this->getCurrent("DS_PWR") << "A";
  LOG(INFO) << "\tBus power  : " << this->getPower("DS_PWR") << "W";
}

void dSiPMDevice::configure() {
  LOG(INFO) << "Configuring";
  configureClock(_config.Get<bool>("clock_internal", true));
  // reset();
  mDelay(10);

  // Configure pixel matrix
  if(!matrixConfigured_) {
    // Read matrix file from the configuration and program it:
    std::string maskFileName = _config.Get("mask_filename", "");
    if(!maskFileName.empty()) {
      LOG(INFO) << "Found mask file name in configuration: \"" << maskFileName << "\"...";
      configureMatrix(maskFileName);
    } else {
      LOG(INFO) << "No mask file name found in configuration.";
    }
  } else {
    LOG(INFO) << "Matrix was already configured. Skipping.";
  }

  // Write initialization settings
  for(auto name : _registers.getNames()) {
    // only initialization registers
    if(std::strcmp(name.c_str(), "validcntr_") > 0 || std::strcmp(name.c_str(), "tdc_") > 0 ||
       std::strcmp(name.c_str(), "set2bit") == 0) {
      // need to read back default values from _registers and use if not given in config
      LOG(DEBUG) << "Setting " << name << " to " << std::hex << _config.Get(name, _registers.get(name).value());
      // TODO this doesna work yet
      // this->setRegister(name, _config.Get(name, _registers.get(name).value()));
    } else {
      LOG(DEBUG) << "Initialization: Skipping " << name;
    }
  }

  // TODO initialize
  // GL_RST (starts initialization, 3.3-V voltage level)
  // SCL, SDA_M (SCL stops with SDA_M, 1.8-V voltage level)
  // SDA_T only for interest at measurements with test circuit
}

template <typename Enumeration> auto as_value(Enumeration const value) -> typename std::underlying_type<Enumeration>::type {
  return static_cast<typename std::underlying_type<Enumeration>::type>(value);
}

void dSiPMDevice::configureMatrix(std::string filename) {

  // Read mask from file
  if(!filename.empty()) {
    LOG(DEBUG) << "Configuring the pixel matrix from mask file \"" << filename << "\"";
    rowMasks = readMask(filename);
  }

  // Write mask to carboard memory (try a few times)
  int retry = 0;
  int retry_max = _config.Get<int>("retry_matrix_config", 3);
  while(true) {
    try {
      programMask();
      break;
    } catch(caribou::DataException& e) {
      LOG(ERROR) << e.what();
      if(++retry == retry_max) {
        throw CommunicationError("Matrix configuration failed");
      }
      LOG(INFO) << "Repeating configuration attempt";
    }
  }

  matrixConfigured_ = true;
}

void dSiPMDevice::programMask() {

  // CLICTD does some checks here... do we need an equivalent?
  // -> Check compression?
  // -> Check for running/ stoped readout clock?
  // -> Read back settings?

  // write mask configuration row by row
  size_t row = totalNRows_;
  while(row > 0) {
    row--;
    std::string rowname("PixelEnable_Row" + std::to_string(row));

    size_t rowmask;
    try { // handle reading errors
      rowmask = rowMasks.at(row);
    } catch(const std::out_of_range& oor) {
      LOG(DEBUG) << "No mask for row " << row << ", using default";
      rowmask = _registers.get(rowname).value();
    }

    LOG(DEBUG) << "Writing row mask " << row << ": " << std::hex << rowmask << " to " << rowname;

    // TODO this does not work yet...
    // this->setRegister(rowname, rowmask);
  }

  return;
}

dSiPMDevice::rowMasks_t dSiPMDevice::readMask(std::string filename) {

  rowMasks_t rowmasks;
  size_t masked = 0;
  bool readingError = false;
  LOG(DEBUG) << "Reading mask file.";
  std::ifstream maskfile(filename);
  if(!maskfile.is_open()) {
    throw ConfigInvalid("Could not open mask file \"" + filename + "\"");
  }

  std::string line = "";
  while(std::getline(maskfile, line)) {

    // Skip empty and commented lines
    if(!line.length() || '#' == line.at(0)) {
      continue;
    }

    // read lines
    std::istringstream maskline(line);
    std::string tag;
    size_t row, val;
    size_t mask = 0;
    size_t cnt = 0;

    if(maskline >> tag >> row) {

      // check for row tag
      if(std::strcmp(tag.c_str(), "row")) {
        continue;
      }

      // read mask values
      while(maskline >> val) {
        // build row mask
        mask = mask << 1;
        mask |= static_cast<bool>(val);
        if(val == 0) {
          masked += val;
        }
        cnt++;
      }

      // further format checks
      if(row > totalNRows_ - 1)
        readingError = true;
      if(cnt > totalNCols_)
        readingError = true;

      // store to mask type
      rowmasks.emplace(row, mask);

    } // maskline

  } // lines

  if(readingError) {
    LOG(ERROR) << "Wrong mask file format return empty mask";
    return rowMasks_t{};
  }

  LOG(INFO) << rowmasks.size() << " row masks cached, " << masked << " pixels are masked";

  return rowmasks;
}

dSiPMDevice::~dSiPMDevice() {
  LOG(INFO) << "Shutdown, delete device.";
  powerOff();
}

void dSiPMDevice::configureClock(bool internal) {

  LOG(DEBUG) << "Configuring Si5345 clock source";
  _hal->configureSI5345(si5345_revb_registers, SI5345_REVB_REG_CONFIG_NUM_REGS);
  mDelay(100); // let the PLL lock

  // If required, check whether we are locked to external clock:
  if(!internal) {
    LOG(DEBUG) << "Waiting for clock to lock...";
    // Try for a limited time to lock, otherwise abort:
    std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();
    while(!_hal->isLockedSI5345()) {
      auto dur = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::steady_clock::now() - start);
      if(dur.count() > 3)
        throw DeviceException("Cannot lock to external clock.");
    }
  }
}

void dSiPMDevice::disableClock() {
  _hal->disableSI5345();
}

void dSiPMDevice::ledOn() { // arguments?? Delay, LED_TRIGGER settings etc.
  LOG(INFO) << "LED ON";

  LOG(DEBUG) << " LED_PWR: " << _config.Get("led_pwr", dSiPM_LED_PWR) << "V";
  this->setVoltage("LED_PWR", _config.Get("led_pwr", dSiPM_LED_PWR), _config.Get("led_pwr_current", dSiPM_LED_PWR_CURRENT));
  this->switchOn("LED_PWR");

  // TODO set led LED_TRIGGER and pulse delay
}

void dSiPMDevice::ledOff() {
  LOG(INFO) << "LED OFF";

  // stop led LED_TRIGGER ?

  LOG(DEBUG) << "Power off LED_PWR";
  this->switchOff("LED_PWR");
}

void dSiPMDevice::powerUp() {
  LOG(INFO) << "Powering up";

  LOG(INFO) << "Power ON";

  LOG(DEBUG) << " VDDIO3V3: " << _config.Get("vddio3v3", dSiPM_VDDIO3V3) << "V";
  this->setVoltage(
    "VDDIO3V3", _config.Get("vddio3v3", dSiPM_VDDIO3V3), _config.Get("vddio3v3_current", dSiPM_VDDIO3V3_CURRENT));
  this->switchOn("VDDIO3V3");

  // Wait a bit
  usleep(100000);

  LOG(DEBUG) << " VDDIO1V8: " << _config.Get("vddio1v8", dSiPM_VDDIO1V8) << "V";
  this->setVoltage(
    "VDDIO1V8", _config.Get("vddio1v8", dSiPM_VDDIO1V8), _config.Get("vddio1v8_current", dSiPM_VDDIO1V8_CURRENT));
  this->switchOn("VDDIO1V8");

  // Wait a bit
  usleep(100000);

  LOG(DEBUG) << " VDDCORE: " << _config.Get("vddcore", dSiPM_VDDCORE) << "V";
  this->setVoltage("VDDCORE", _config.Get("vddcore", dSiPM_VDDCORE), _config.Get("vddcore_current", dSiPM_VDDCORE_CURRENT));
  this->switchOn("VDDCORE");

  // Wait a bit
  usleep(100000);

  LOG(DEBUG) << " DS_PWR: " << _config.Get("ds_pwr", dSiPM_DS_PWR) << "V";
  this->setVoltage("DS_PWR", _config.Get("ds_pwr", dSiPM_DS_PWR), _config.Get("ds_pwr_current", dSiPM_DS_PWR_CURRENT));
  this->switchOn("DS_PWR");

  // Wait a bit
  usleep(100000);

  LOG(DEBUG) << " CMOS_OUT_1_TO_4: " << CMOS_OUT_1_TO_4_VOLTAGE << "V";
  this->setVoltage("CMOS_OUT_1_TO_4", CMOS_OUT_1_TO_4_VOLTAGE);
  this->switchOn("CMOS_OUT_1_TO_4");

  LOG(DEBUG) << " CMOS_OUT_5_TO_8: " << CMOS_OUT_5_TO_8_VOLTAGE << "V";
  this->setVoltage("CMOS_OUT_5_TO_8", CMOS_OUT_5_TO_8_VOLTAGE);
  this->switchOn("CMOS_OUT_5_TO_8");

  // Wait a bit
  usleep(500000);

  LOG(INFO) << "Starting clock -- TODO";

  /*
  3-MHz BUNCH_CLOCK
  408-MHz system CLOCK
  FRAME_RST
  READ
  */

  // Wait a bit
  usleep(500000);

  LOG(INFO) << "Initializing chip -- TODO";

  /*
  GL_RST
  SCL
  SDA_M
  (SDA_T)
  */

  // Wait a bit
  usleep(500000);

  LOG(INFO) << "BIAS ON";

  LOG(DEBUG) << " BIAS_2 (VCLAMP): " << _config.Get("bias_2", dSiPM_BIAS_2) << "V";
  this->setVoltage("BIAS_2", _config.Get("bias_2", dSiPM_BIAS_2));
  this->switchOn("BIAS_2");

  // Wait a bit
  usleep(100000);

  LOG(DEBUG) << " BIAS_4 (VBQ): " << _config.Get("bias_4", dSiPM_BIAS_4) << "V";
  this->setVoltage("BIAS_4", _config.Get("bias_4", dSiPM_BIAS_4));
  this->switchOn("BIAS_4");

  // Wait a bit
  usleep(100000);

  LOG(DEBUG) << " BIAS_1: " << _config.Get("bias_1", dSiPM_BIAS_1) << "V";
  this->setVoltage("BIAS_1", _config.Get("bias_1", dSiPM_BIAS_1));
  this->switchOn("BIAS_1");

  // Wait a bit
  usleep(100000);

  LOG(DEBUG) << " BIAS_3: " << _config.Get("bias_3", dSiPM_BIAS_3) << "V";
  this->setVoltage("BIAS_3", _config.Get("bias_3", dSiPM_BIAS_3));
  this->switchOn("BIAS_3");

  // Wait a bit
  usleep(500000);

  LOG(INFO) << "HV ON -- TODO";

  /*
  swhitch ON HV (range 18-22V)
  */

  // Wait a bit
  usleep(500000);

  LOG(INFO) << "Control Signals ON -- TODO";

  /*
  DS_EQ
  DS_PE
  DS_PD
  */

  // Wait a bit
  usleep(500000);

  if(_config.Get("led_on", dSiPM_LED_On)) {
    ledOn();
  }

  // Wait a bit
  usleep(500000);

  return;
}

void dSiPMDevice::powerDown() {
  LOG(INFO) << "Powering down";

  LOG(INFO) << "Stop measurement -- TODO";

  // Wait a bit
  usleep(500000);

  ledOff();

  // Wait a bit
  usleep(500000);

  LOG(INFO) << "Control Signal OFF -- TODO";

  // Wait a bit
  usleep(500000);

  LOG(INFO) << "HV OFF -- TODO";

  // Wait a bit
  usleep(500000);

  LOG(INFO) << "BIAS OFF";

  LOG(DEBUG) << "Power off BIAS_3";
  this->switchOff("BIAS_3");

  // Wait a bit
  usleep(100000);

  LOG(DEBUG) << "Power off BIAS_1";
  this->switchOff("BIAS_1");

  // Wait a bit
  usleep(100000);

  LOG(DEBUG) << "Power off BIAS_4 (VBQ)";
  this->switchOff("BIAS_4");

  // Wait a bit
  usleep(100000);

  LOG(DEBUG) << "Power off BIAS_2 (VCLAMP)";
  this->switchOff("BIAS_2");

  // Wait a bit
  usleep(500000);

  LOG(DEBUG) << "Power off CMOS signals level shifters";
  this->switchOff("CMOS_OUT_1_TO_4");
  this->switchOff("CMOS_OUT_1_TO_4");

  // Wait a bit
  usleep(500000);

  LOG(INFO) << "Chip OFF -- TODO";

  // Wait a bit
  usleep(500000);

  LOG(INFO) << "Clock OFF -- TODO";

  // Wait a bit
  usleep(500000);

  LOG(INFO) << "Power OFF";

  LOG(DEBUG) << "Power off DS_PWR";
  this->switchOff("DS_PWR");

  // Wait a bit
  usleep(100000);

  LOG(DEBUG) << "Power off VDDCORE";
  this->switchOff("VDDCORE");

  // Wait a bit
  usleep(100000);

  LOG(DEBUG) << "Power off VDDIO1V8";
  this->switchOff("VDDIO1V8");

  // Wait a bit
  usleep(100000);

  LOG(DEBUG) << "Power off VDDIO3V3";
  this->switchOff("VDDIO3V3");

  return;
}

pearyRawData dSiPMDevice::getFrame() {

  LOG(ERROR) << "dSiPMDevice::getFrame() not yet implemented. Output is empty.";

  // In CLICKTD we have something like manual readout. I suppose we do not need that for now!

  // Check if there is something available
  uint32_t attempts = 0;
  // TODO what do WE check here?
  // while(!(getMemory("fifostatus") & 0x1))
  while(true) {
    // TODO how much delay do WE need?
    usleep(100);
    if(attempts++ >= 10) {
      // nope, FIFO is still empty.
      LOG(DEBUG) << "No data available in the readout FIFO.";
      throw NoDataAvailable();
    }
  }
  // If so, continue with read out

  // Read back data structure TODO find out how it is stuctured
  uintptr_t data;
  uintptr_t data_count = 0;
  uintptr_t ts_count = 0;
  pearyRawData rawdata;

  // read data
  LOG(DEBUG) << "Reading " << data_count << " words from FIFO";
  while(data_count--) {
    data = getMemory("fifodata_msb");
    rawdata.push_back(data);
    data = getMemory("fifodata_lsb");
    rawdata.push_back(data);
  }
  // read timestamps
  while(ts_count--) {
    data = getMemory("fifodata_msb");
    rawdata.push_back(data);
    data = getMemory("fifodata_lsb");
    rawdata.push_back(data);
  }

  return rawdata;
}

pearyRawData dSiPMDevice::getRawData() {
  return getFrame();
}

pearyRawData dSiPMDevice::getDeviceData() {

  LOG(ERROR) << "dSiPMDevice::getDeviceData() not yet implemented. Output is empty.";

  // This is used to strip the timestamps in CLICTD... are we going to need this?

  return pearyRawData();
}

pearydata dSiPMDevice::getData() {
  LOG(DEBUG) << "dSiPMDevice::getData(): Starting to decode frame.";
  auto rawdata = getDeviceData();
  return frame_decoder_.decodeFrame<bool>(rawdata, 0);
}
