#ifndef DSIPM_FRAMEDECODER_HPP
#define DSIPM_FRAMEDECODER_HPP

#include <vector>

#include "utils/datatypes.hpp"
#include "utils/log.hpp"
#include "utils/utils.hpp"

#include "dSiPMPixels.hpp"

#define DSIPM_N_QUADCOL 16                   // columns per quadrant
#define DSIPM_N_QUADROW 16                   // rows per quadrant
#define DSIPM_N_COL 2 * DSIPM_N_QUADCOL      //   32 -- all columns
#define DSIPM_N_ROW 2 * DSIPM_N_QUADROW      //   32 -- all rows
#define DSIPM_N_PIX DSIPM_N_COL* DSIPM_N_ROW // 1024 -- all pixels

namespace caribou {

  class dSiPMFrameDecoder {
  public:
    dSiPMFrameDecoder() : m_indexToCol{DSIPM_N_QUADCOL}, m_indexToRow{DSIPM_N_QUADROW} { generateMapping(); };

    template <typename T> pearydata decodeFrame(const pearyRawData& rawFrame, bool suppressZero = false);

    typedef size_t index_type;

  private:
    /* Funktions to generate lookup tables.
     * These map between the pixels indices in the raw data stream and
     * the pixel column and row indices in the full matrix.
     */
    void generateMapping();

    /* Get the pixel index inside a quadrant from the index in the raw data stream
     * Suppose the data comes in an array of 1024 entries, like Q1, Q2, Q3, Q4, respectively.
     */
    index_type getIpix(index_type itot);

    /* Get the quadrant a pixel belongs to from the index in the raw data stream
     * Suppose the data comes in an array of 1024 entries, like Q1, Q2, Q3, Q4, respectively.
     */
    index_type getQuad(index_type itot);

    /* Map the pixel index in a quadrant to column and row indices in a quadrant.
     */
    index_type getQuadCol(index_type ipix);
    index_type getQuadRow(index_type ipix);

    /* Map the column and row indices within a quadrant to their global correspondance.
     */
    index_type getCol(index_type ipix, index_type quad);
    index_type getRow(index_type ipix, index_type quad);

    /* Lookup tables
     */
    index_type m_indexToCol[1024];
    index_type m_indexToRow[1024];
  };

} // namespace caribou

#endif
