#include "dSiPMFrameDecoder.hpp"
#include "utils/lfsr.hpp"

using namespace caribou;

// simplifies getIpix
const dSiPMFrameDecoder::index_type rowBy8 = DSIPM_N_QUADROW / 4; //  4
const dSiPMFrameDecoder::index_type rowBy2 = DSIPM_N_QUADROW;     // 16
const dSiPMFrameDecoder::index_type rowTm2 = DSIPM_N_QUADROW * 4; // 64

// TODO do we need to use any other data type than bool?
template <> pearydata dSiPMFrameDecoder::decodeFrame<uint64_t>(const pearyRawData&, bool) {
  return pearydata();
}

// TODO will this be bool? or do we do some memcp magic like for DSO9254A?
template <> pearydata dSiPMFrameDecoder::decodeFrame<bool>(const pearyRawData& rawFrame, bool suppressZero) {
  pearydata data;

  // TODO do we have some check like that?
  /*
  if(getNextPixel<uint32_t>(rawFrame, wrd, bit) != CLICTD_FRAME_START) {
    LOG(ERROR) << "The first word does not match the frame start pattern.";
    return data;
  }
  */

  // The raw data structure will be simple:
  // An array off booleans, where the array index can be maped to the pixel position
  // Or is this going to be maped into some other data type?
  for(index_type i = 0; i < rawFrame.size(); i++) {

    // get data
    bool value = rawFrame.at(i);
    index_type col = m_indexToCol[i];
    index_type row = m_indexToRow[i];

    // Suppress empty pixels, if we wish to do so.
    if(suppressZero == true && value == 0) {
      continue;
    }

    // Fill data structure.
    data[std::make_pair(col, row)] = std::make_unique<dsipm_pixel>(value);
  }

  // TODO do we have some check like that?
  /*
  if(getNextPixel<uint32_t>(rawFrame, wrd, bit) != CLICTD_FRAME_END) {
    LOG(ERROR) << "The last word does not match the frame end pattern.";
    return data;
  }
  */

  return data;
}

void dSiPMFrameDecoder::generateMapping() {
  LOG(INFO) << "Generatring lokup tables for pixel mapping.";

  for(index_type i = 0; i < DSIPM_N_PIX; i++) {
    m_indexToCol[i] = getCol(getIpix(i), getQuad(i));
    m_indexToRow[i] = getRow(getIpix(i), getQuad(i));
  }

  return;
}

dSiPMFrameDecoder::index_type dSiPMFrameDecoder::getIpix(index_type itot) {
  // Reduce to one pixel matrix.
  index_type i = itot % (DSIPM_N_QUADCOL * DSIPM_N_QUADCOL);

  // Map index to pixel number.
  //                |----------- jump 4 rows ----------| + |--- jump 1 row --| - |---- next column ----|
  index_type ipix = ((1 + rowBy8 * (i % rowBy8)) * rowBy2) + rowBy2 * (i / rowBy8) - (rowTm2 + 1) * (i / rowBy2);

  return ipix;
}

dSiPMFrameDecoder::index_type dSiPMFrameDecoder::getQuad(index_type itot) {
  // Calculate quadrant index.
  return itot / (DSIPM_N_QUADCOL * DSIPM_N_QUADCOL);
}

dSiPMFrameDecoder::index_type dSiPMFrameDecoder::getQuadCol(index_type ipix) {
  // Map quadrant index to quadrant column.
  return (ipix - 1) % DSIPM_N_QUADCOL;
}

dSiPMFrameDecoder::index_type dSiPMFrameDecoder::getQuadRow(index_type ipix) {
  // Map quadrant index to quadrant row.
  return (ipix - 1) / DSIPM_N_QUADCOL;
}

dSiPMFrameDecoder::index_type dSiPMFrameDecoder::getCol(index_type ipix, index_type quad) {
  // Map quadrant column to global column/
  index_type col;
  switch(quad) {
  case index_type(0):
    col = DSIPM_N_QUADCOL - 1 - getQuadCol(ipix);
    break;
  case index_type(1):
    col = DSIPM_N_QUADCOL + getQuadCol(ipix);
    break;
  case index_type(2):
    col = DSIPM_N_QUADCOL - 1 - getQuadCol(ipix);
    break;
  case index_type(3):
    col = DSIPM_N_QUADCOL + getQuadCol(ipix);
    break;
  default:
    LOG(ERROR) << "Conversion error in getCol, quadrant " << quad << " not defined.";
    col = DSIPM_N_COL; // This will be out of boundaries...
  }
  return col;
}

dSiPMFrameDecoder::index_type dSiPMFrameDecoder::getRow(index_type ipix, index_type quad) {
  // Map quadrant row to global row.
  index_type row;
  switch(quad) {
  case index_type(0):
    row = 2 * DSIPM_N_QUADROW - 1 - getQuadRow(ipix);
    break;
  case index_type(1):
    row = 2 * DSIPM_N_QUADROW - 1 - getQuadRow(ipix);
    break;
  case index_type(2):
    row = DSIPM_N_QUADROW - 1 - getQuadRow(ipix);
    break;
  case index_type(3):
    row = DSIPM_N_QUADROW - 1 - getQuadRow(ipix);
    break;
  default:
    LOG(ERROR) << "Conversion error in getRow, quadrant " << quad << " not defined.";
    row = DSIPM_N_ROW; // This will be out of boundaries...
  }
  return row;
}
