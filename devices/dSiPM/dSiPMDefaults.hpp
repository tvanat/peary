#ifndef DEVICE_DSIPM_DEFAULTS_H
#define DEVICE_DSIPM_DEFAULTS_H

#include "utils/dictionary.hpp"

namespace caribou {

// define voltages
#define dSiPM_VDDIO3V3 3.3
#define dSiPM_VDDIO3V3_CURRENT 0.2
#define dSiPM_VDDIO1V8 1.8
#define dSiPM_VDDIO1V8_CURRENT 0.1
#define dSiPM_VDDCORE 1.8
#define dSiPM_VDDCORE_CURRENT 0.1
#define dSiPM_LED_PWR 3.6
#define dSiPM_LED_PWR_CURRENT 0.15
#define dSiPM_LED_On false
#define dSiPM_DS_PWR 3.3
#define dSiPM_DS_PWR_CURRENT 0.6

#define dSiPM_BIAS_1 0.5 // should range 0.5 to 2.5 V
#define dSiPM_BIAS_2 1.8 // should range 1.8 to 2.5 V
#define dSiPM_BIAS_3 1.8 // should range 1.8 to 2.5 V
#define dSiPM_BIAS_4 0.5 // should range 0.5 to 2.5 V
#define CMOS_OUT_1_TO_4_VOLTAGE 1.8
#define CMOS_OUT_5_TO_8_VOLTAGE 3.3

  // DSIPM FPGA address space
  const intptr_t DSIPM_CORE_BASE_ADDRESS = 0x43C70000;
  const intptr_t DSIPM_CORE_LSB = 2;
  const size_t DSIPM_READOUT_MAP_SIZE = 4096;

  // Assuming we start with the 32 bit for initialization settings,
  // followed by 32 x 32 bit for pixel masking (see)
  const size_t DSIPM_CHIP_BASE_ADDR = DSIPM_CORE_BASE_ADDRESS + 0x000;
  const size_t DSIPM_CTRL_BASE_ADDR = DSIPM_CORE_BASE_ADDRESS + 0x100;
  const size_t DSIPM_ROUT_BASE_ADDR = DSIPM_CORE_BASE_ADDRESS + 0x200;

  // DSIPM pattern generator output signals
  // TODO, put correct values
#define DSIPM_READOUT_START 0x01
#define DSIPM_SHUTTER 0x00
#define DSIPM_LED 0x00

  // TODO, check if this is what we want
  const memory_map DSIPM_READOUT{DSIPM_CORE_BASE_ADDRESS, DSIPM_READOUT_MAP_SIZE, PROT_READ | PROT_WRITE};

  // clang-format off

#define DSIPM_MEMORY                                                                             \
  {                                                                                              \
    {"c00", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>( 0 << DSIPM_CORE_LSB)}},  \
    {"c01", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>( 1 << DSIPM_CORE_LSB)}},  \
    {"c02", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>( 2 << DSIPM_CORE_LSB)}},  \
    {"c03", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>( 3 << DSIPM_CORE_LSB)}},  \
    {"c04", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>( 4 << DSIPM_CORE_LSB)}},  \
    {"c05", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>( 5 << DSIPM_CORE_LSB)}},  \
    {"c06", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>( 6 << DSIPM_CORE_LSB)}},  \
    {"c07", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>( 7 << DSIPM_CORE_LSB)}},  \
    {"c08", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>( 8 << DSIPM_CORE_LSB)}},  \
    {"c09", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>( 9 << DSIPM_CORE_LSB)}},  \
    {"c10", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>(10 << DSIPM_CORE_LSB)}},  \
    {"c11", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>(11 << DSIPM_CORE_LSB)}},  \
    {"c12", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>(12 << DSIPM_CORE_LSB)}},  \
    {"c13", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>(13 << DSIPM_CORE_LSB)}},  \
    {"c14", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>(14 << DSIPM_CORE_LSB)}},  \
    {"c15", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>(15 << DSIPM_CORE_LSB)}},  \
    {"c16", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>(16 << DSIPM_CORE_LSB)}},  \
    {"c17", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>(17 << DSIPM_CORE_LSB)}},  \
    {"c18", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>(18 << DSIPM_CORE_LSB)}},  \
    {"c19", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>(19 << DSIPM_CORE_LSB)}},  \
    {"c20", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>(20 << DSIPM_CORE_LSB)}},  \
    {"c21", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>(21 << DSIPM_CORE_LSB)}},  \
    {"c22", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>(22 << DSIPM_CORE_LSB)}},  \
    {"c23", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>(23 << DSIPM_CORE_LSB)}},  \
    {"c24", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>(24 << DSIPM_CORE_LSB)}},  \
    {"c25", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>(25 << DSIPM_CORE_LSB)}},  \
    {"c26", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>(26 << DSIPM_CORE_LSB)}},  \
    {"c27", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>(27 << DSIPM_CORE_LSB)}},  \
    {"c28", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>(28 << DSIPM_CORE_LSB)}},  \
    {"c29", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>(29 << DSIPM_CORE_LSB)}},  \
    {"c30", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>(30 << DSIPM_CORE_LSB)}},  \
    {"c31", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>(31 << DSIPM_CORE_LSB)}},  \
    {"c32", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>(32 << DSIPM_CORE_LSB)}},  \
    {"c33", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>(33 << DSIPM_CORE_LSB)}},  \
    {"f00", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>(64 << DSIPM_CORE_LSB)}},  \
    {"f01", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>(65 << DSIPM_CORE_LSB)}},  \
    {"f02", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>(66 << DSIPM_CORE_LSB)}},  \
    {"f03", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>(67 << DSIPM_CORE_LSB)}},  \
    {"f04", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>(68 << DSIPM_CORE_LSB)}},  \
    {"f05", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>(69 << DSIPM_CORE_LSB)}},  \
    {"f06", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>(70 << DSIPM_CORE_LSB)}},  \
    {"f07", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>(71 << DSIPM_CORE_LSB)}},  \
    {"f08", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>(72 << DSIPM_CORE_LSB)}},  \
    {"f09", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>(73 << DSIPM_CORE_LSB)}},  \
    {"r00", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>(128 << DSIPM_CORE_LSB)}},  \
    {"r01", {DSIPM_READOUT, register_t<std::uintptr_t, std::uintptr_t>(129 << DSIPM_CORE_LSB)}}   \
  }

  // clang-format on

  /** Dictionary for register address/name lookup for DSIPM
   */

  // clang-format off

#define DSIPM_REGISTERS                                                                                                     \
  {                                                                                                                         \
    {"ValidCntr_Q1",       register_t<size_t>(DSIPM_CHIP_BASE_ADDR+( 0 << DSIPM_CORE_LSB), 0x000000F0, 0x0       , true, true, false)}, \
    {"TDC_Q1",             register_t<size_t>(DSIPM_CHIP_BASE_ADDR+( 0 << DSIPM_CORE_LSB), 0x00000300, 0x1       , true, true, false)}, \
    {"ValidCntr_Q3",       register_t<size_t>(DSIPM_CHIP_BASE_ADDR+( 0 << DSIPM_CORE_LSB), 0x00003C00, 0x0       , true, true, false)}, \
    {"TDC_Q3",             register_t<size_t>(DSIPM_CHIP_BASE_ADDR+( 0 << DSIPM_CORE_LSB), 0x0000C000, 0x1       , true, true, false)}, \
    {"set2Bit",            register_t<size_t>(DSIPM_CHIP_BASE_ADDR+( 0 << DSIPM_CORE_LSB), 0x00010000, 0x1       , true, true, false)}, \
    {"ValidCntr_Q2",       register_t<size_t>(DSIPM_CHIP_BASE_ADDR+( 0 << DSIPM_CORE_LSB), 0x001E0000, 0x0       , true, true, false)}, \
    {"TDC_Q2",             register_t<size_t>(DSIPM_CHIP_BASE_ADDR+( 0 << DSIPM_CORE_LSB), 0x00600000, 0x1       , true, true, false)}, \
    {"ValidCntr_Q4",       register_t<size_t>(DSIPM_CHIP_BASE_ADDR+( 0 << DSIPM_CORE_LSB), 0x07800000, 0x0       , true, true, false)}, \
    {"TDC_Q4",             register_t<size_t>(DSIPM_CHIP_BASE_ADDR+( 0 << DSIPM_CORE_LSB), 0x18000000, 0x1       , true, true, false)}, \
    {"PixelEnable_ROW00",  register_t<size_t>(DSIPM_CHIP_BASE_ADDR+( 1 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, true, false)}, \
    {"PixelEnable_ROW01",  register_t<size_t>(DSIPM_CHIP_BASE_ADDR+( 2 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, true, false)}, \
    {"PixelEnable_ROW02",  register_t<size_t>(DSIPM_CHIP_BASE_ADDR+( 3 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, true, false)}, \
    {"PixelEnable_ROW03",  register_t<size_t>(DSIPM_CHIP_BASE_ADDR+( 4 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, true, false)}, \
    {"PixelEnable_ROW04",  register_t<size_t>(DSIPM_CHIP_BASE_ADDR+( 5 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, true, false)}, \
    {"PixelEnable_ROW05",  register_t<size_t>(DSIPM_CHIP_BASE_ADDR+( 6 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, true, false)}, \
    {"PixelEnable_ROW06",  register_t<size_t>(DSIPM_CHIP_BASE_ADDR+( 7 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, true, false)}, \
    {"PixelEnable_ROW07",  register_t<size_t>(DSIPM_CHIP_BASE_ADDR+( 8 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, true, false)}, \
    {"PixelEnable_ROW08",  register_t<size_t>(DSIPM_CHIP_BASE_ADDR+( 9 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, true, false)}, \
    {"PixelEnable_ROW09",  register_t<size_t>(DSIPM_CHIP_BASE_ADDR+(10 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, true, false)}, \
    {"PixelEnable_ROW10",  register_t<size_t>(DSIPM_CHIP_BASE_ADDR+(11 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, true, false)}, \
    {"PixelEnable_ROW11",  register_t<size_t>(DSIPM_CHIP_BASE_ADDR+(12 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, true, false)}, \
    {"PixelEnable_ROW12",  register_t<size_t>(DSIPM_CHIP_BASE_ADDR+(13 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, true, false)}, \
    {"PixelEnable_ROW13",  register_t<size_t>(DSIPM_CHIP_BASE_ADDR+(14 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, true, false)}, \
    {"PixelEnable_ROW14",  register_t<size_t>(DSIPM_CHIP_BASE_ADDR+(15 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, true, false)}, \
    {"PixelEnable_ROW15",  register_t<size_t>(DSIPM_CHIP_BASE_ADDR+(16 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, true, false)}, \
    {"PixelEnable_ROW16",  register_t<size_t>(DSIPM_CHIP_BASE_ADDR+(17 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, true, false)}, \
    {"PixelEnable_ROW17",  register_t<size_t>(DSIPM_CHIP_BASE_ADDR+(18 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, true, false)}, \
    {"PixelEnable_ROW18",  register_t<size_t>(DSIPM_CHIP_BASE_ADDR+(19 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, true, false)}, \
    {"PixelEnable_ROW19",  register_t<size_t>(DSIPM_CHIP_BASE_ADDR+(20 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, true, false)}, \
    {"PixelEnable_ROW20",  register_t<size_t>(DSIPM_CHIP_BASE_ADDR+(21 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, true, false)}, \
    {"PixelEnable_ROW21",  register_t<size_t>(DSIPM_CHIP_BASE_ADDR+(22 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, true, false)}, \
    {"PixelEnable_ROW22",  register_t<size_t>(DSIPM_CHIP_BASE_ADDR+(23 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, true, false)}, \
    {"PixelEnable_ROW23",  register_t<size_t>(DSIPM_CHIP_BASE_ADDR+(24 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, true, false)}, \
    {"PixelEnable_ROW24",  register_t<size_t>(DSIPM_CHIP_BASE_ADDR+(25 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, true, false)}, \
    {"PixelEnable_ROW25",  register_t<size_t>(DSIPM_CHIP_BASE_ADDR+(26 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, true, false)}, \
    {"PixelEnable_ROW26",  register_t<size_t>(DSIPM_CHIP_BASE_ADDR+(27 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, true, false)}, \
    {"PixelEnable_ROW27",  register_t<size_t>(DSIPM_CHIP_BASE_ADDR+(28 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, true, false)}, \
    {"PixelEnable_ROW28",  register_t<size_t>(DSIPM_CHIP_BASE_ADDR+(29 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, true, false)}, \
    {"PixelEnable_ROW29",  register_t<size_t>(DSIPM_CHIP_BASE_ADDR+(30 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, true, false)}, \
    {"PixelEnable_ROW30",  register_t<size_t>(DSIPM_CHIP_BASE_ADDR+(31 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, true, false)}, \
    {"PixelEnable_ROW31",  register_t<size_t>(DSIPM_CHIP_BASE_ADDR+(32 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, true, false)}, \
    \
    {"daq_start_tlu",      register_t<size_t>(DSIPM_CTRL_BASE_ADDR+( 0 << DSIPM_CORE_LSB), 0x00000001, 0x0,        true, true, false)}, \
    {"daq_start_stdalone", register_t<size_t>(DSIPM_CTRL_BASE_ADDR+( 0 << DSIPM_CORE_LSB), 0x00000002, 0x0,        true, true, false)}, \
    {"daq_stop",           register_t<size_t>(DSIPM_CTRL_BASE_ADDR+( 0 << DSIPM_CORE_LSB), 0x00000004, 0x0,        true, true, false)}, \
    {"mode_2bit",          register_t<size_t>(DSIPM_CTRL_BASE_ADDR+( 0 << DSIPM_CORE_LSB), 0x00000008, 0x0,        true, true, false)}, \
    {"hv_enable",          register_t<size_t>(DSIPM_CTRL_BASE_ADDR+( 1 << DSIPM_CORE_LSB), 0x00000001, 0x0,        true, true, false)}, \
    {"led_trigger",        register_t<size_t>(DSIPM_CTRL_BASE_ADDR+( 1 << DSIPM_CORE_LSB), 0x00000002, 0x0,        true, true, false)}, \
    {"ds_pd",              register_t<size_t>(DSIPM_CTRL_BASE_ADDR+( 1 << DSIPM_CORE_LSB), 0x00000004, 0x0,        true, true, false)}, \
    {"ds_eq",              register_t<size_t>(DSIPM_CTRL_BASE_ADDR+( 1 << DSIPM_CORE_LSB), 0x00000008, 0x0,        true, true, false)}, \
    {"ds_pe",              register_t<size_t>(DSIPM_CTRL_BASE_ADDR+( 1 << DSIPM_CORE_LSB), 0x00000010, 0x0,        true, true, false)}, \
    \
    {"rd_data_available",  register_t<size_t>(DSIPM_ROUT_BASE_ADDR+( 0 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, false, false)}, \
    {"rd_data_00",         register_t<size_t>(DSIPM_ROUT_BASE_ADDR+( 1 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, false, false)}, \
    {"rd_data_01",         register_t<size_t>(DSIPM_ROUT_BASE_ADDR+( 2 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, false, false)}, \
    {"rd_data_02",         register_t<size_t>(DSIPM_ROUT_BASE_ADDR+( 3 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, false, false)}, \
    {"rd_data_03",         register_t<size_t>(DSIPM_ROUT_BASE_ADDR+( 4 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, false, false)}, \
    {"rd_data_04",         register_t<size_t>(DSIPM_ROUT_BASE_ADDR+( 5 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, false, false)}, \
    {"rd_data_05",         register_t<size_t>(DSIPM_ROUT_BASE_ADDR+( 6 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, false, false)}, \
    {"rd_data_06",         register_t<size_t>(DSIPM_ROUT_BASE_ADDR+( 7 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, false, false)}, \
    {"rd_data_07",         register_t<size_t>(DSIPM_ROUT_BASE_ADDR+( 8 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, false, false)}, \
    {"rd_data_08",         register_t<size_t>(DSIPM_ROUT_BASE_ADDR+( 9 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, false, false)}, \
    {"rd_data_09",         register_t<size_t>(DSIPM_ROUT_BASE_ADDR+(10 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, false, false)}, \
    {"rd_data_10",         register_t<size_t>(DSIPM_ROUT_BASE_ADDR+(11 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, false, false)}, \
    {"rd_data_11",         register_t<size_t>(DSIPM_ROUT_BASE_ADDR+(12 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, false, false)}, \
    {"rd_data_12",         register_t<size_t>(DSIPM_ROUT_BASE_ADDR+(13 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, false, false)}, \
    {"rd_data_13",         register_t<size_t>(DSIPM_ROUT_BASE_ADDR+(14 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, false, false)}, \
    {"rd_data_14",         register_t<size_t>(DSIPM_ROUT_BASE_ADDR+(15 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, false, false)}, \
    {"rd_data_15",         register_t<size_t>(DSIPM_ROUT_BASE_ADDR+(16 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, false, false)}, \
    {"rd_data_16",         register_t<size_t>(DSIPM_ROUT_BASE_ADDR+(17 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, false, false)}, \
    {"rd_data_17",         register_t<size_t>(DSIPM_ROUT_BASE_ADDR+(18 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, false, false)}, \
    {"rd_data_18",         register_t<size_t>(DSIPM_ROUT_BASE_ADDR+(19 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, false, false)}, \
    {"rd_data_19",         register_t<size_t>(DSIPM_ROUT_BASE_ADDR+(20 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, false, false)}, \
    {"rd_data_20",         register_t<size_t>(DSIPM_ROUT_BASE_ADDR+(21 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, false, false)}, \
    {"rd_data_21",         register_t<size_t>(DSIPM_ROUT_BASE_ADDR+(22 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, false, false)}, \
    {"rd_data_22",         register_t<size_t>(DSIPM_ROUT_BASE_ADDR+(23 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, false, false)}, \
    {"rd_data_23",         register_t<size_t>(DSIPM_ROUT_BASE_ADDR+(24 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, false, false)}, \
    {"rd_data_24",         register_t<size_t>(DSIPM_ROUT_BASE_ADDR+(25 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, false, false)}, \
    {"rd_data_25",         register_t<size_t>(DSIPM_ROUT_BASE_ADDR+(26 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, false, false)}, \
    {"rd_data_26",         register_t<size_t>(DSIPM_ROUT_BASE_ADDR+(27 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, false, false)}, \
    {"rd_data_27",         register_t<size_t>(DSIPM_ROUT_BASE_ADDR+(28 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, false, false)}, \
    {"rd_data_28",         register_t<size_t>(DSIPM_ROUT_BASE_ADDR+(29 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, false, false)}, \
    {"rd_data_29",         register_t<size_t>(DSIPM_ROUT_BASE_ADDR+(30 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, false, false)}, \
    {"rd_data_30",         register_t<size_t>(DSIPM_ROUT_BASE_ADDR+(31 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, false, false)}, \
    {"rd_data_31",         register_t<size_t>(DSIPM_ROUT_BASE_ADDR+(32 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, false, false)}, \
    {"rd_data_32",         register_t<size_t>(DSIPM_ROUT_BASE_ADDR+(33 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, false, false)}, \
    {"rd_data_33",         register_t<size_t>(DSIPM_ROUT_BASE_ADDR+(34 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, false, false)}, \
    {"rd_data_34",         register_t<size_t>(DSIPM_ROUT_BASE_ADDR+(35 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, false, false)}, \
    {"rd_data_35",         register_t<size_t>(DSIPM_ROUT_BASE_ADDR+(36 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, false, false)}, \
    {"rd_data_36",         register_t<size_t>(DSIPM_ROUT_BASE_ADDR+(37 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, false, false)}, \
    {"rd_data_37",         register_t<size_t>(DSIPM_ROUT_BASE_ADDR+(38 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, false, false)}, \
    {"rd_data_38",         register_t<size_t>(DSIPM_ROUT_BASE_ADDR+(39 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, false, false)}, \
    {"rd_data_39",         register_t<size_t>(DSIPM_ROUT_BASE_ADDR+(40 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, false, false)}, \
    {"rd_data_40",         register_t<size_t>(DSIPM_ROUT_BASE_ADDR+(41 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, false, false)}, \
    {"rd_data_41",         register_t<size_t>(DSIPM_ROUT_BASE_ADDR+(42 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, false, false)}, \
    {"rd_data_42",         register_t<size_t>(DSIPM_ROUT_BASE_ADDR+(43 << DSIPM_CORE_LSB), 0xFFFFFFFF, 0xFFFFFFFF, true, false, false)} \
  }

  // clang-format on

} // namespace caribou

#endif /* DEVICE_DSIPM_DEFAULTS_H */
