// Defines CLICTD pixels types

#ifndef DSIPM_PIXELS_HPP
#define DSIPM_PIXELS_HPP

#include <ostream>
#include "utils/datatypes.hpp"
#include "utils/exceptions.hpp"

namespace caribou {

  /* Basic pixel class
   * Binary data, so signal is simplt a boolean
   */
  // TODO add timestamps?
  // TODO For CLICTD this class is virtual, data and constructors are  protected.
  //      Is this fine for our purpose, as we do not need derived classes for configuration?

  class dsipm_pixel : public pixel {
  public:
    dsipm_pixel(){};
    dsipm_pixel(bool s) : m_bit(s){};
    virtual ~dsipm_pixel(){};

    bool getBit() { return m_bit; }

  private:
    bool m_bit;
  };

} // namespace caribou

#endif
