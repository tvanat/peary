/**
 * Caribou implementation for the dSiPM chip
 */

#ifndef DEVICE_DSIPM_H
#define DEVICE_DSIPM_H

#include "device/CaribouDevice.hpp"
#include "hardware_abstraction/carboard/Carboard.hpp"

#include "clockgenerator/Si5345-RevB-dSiPM-Registers.h"
#include "dSiPMDefaults.hpp"
#include "dSiPMFrameDecoder.hpp"
#include "dSiPMPixels.hpp"

namespace caribou {

  /** dSiPM Device class definition
   */
  class dSiPMDevice : public CaribouDevice<carboard::Carboard, iface_mem> {

  public:
    dSiPMDevice(const caribou::Configuration config);
    ~dSiPMDevice();

    /** Initializer function for dSiPM
     */
    void configure() override;

    /** Configure and disable clock
     */
    void configureClock(bool internal = 1);
    void disableClock();

    /** Start and stop daq
     */
    void daqStart() override{};
    void daqStop() override{};

    /** Switch power on and off, list power status
     */
    void powerUp() override;
    void powerDown() override;
    void ledOn();
    void ledOff();
    void powerStatusLog();

    /** Configure pixel matrix from mask file
     */
    void configureMatrix(std::string filename);

    /** Returns data as retrieved from chip
     */
    pearyRawData getRawData() override;
    /** Returns data, striped from timestamp
     */
    pearyRawData getDeviceData();
    /** Retruns data, converted into peary data, using frame decoder
     */
    pearydata getData() override;

  private:
    /** Configure the pixel matrix (masking)
     */
    void programMask();

    bool matrixConfigured_;

    /* Store pixel mask values as register mask type for each row.
     * This conveniently fits the way the registers are defined on
     * the chip.
     */
    using rowMasks_t = std::map<size_t, size_t>;
    rowMasks_t rowMasks{};
    rowMasks_t readMask(std::string filename);

    /** Retrieves data from chip
     */
    pearyRawData getFrame();

    /** Class to handle conversion of pearyrawdata to pearydata
     */
    dSiPMFrameDecoder frame_decoder_;

    /** Chip parameters
     */
    const size_t totalNRows_ = 32;
    const size_t totalNCols_ = 32;
  };

} // namespace caribou

#endif /* DEVICE_DSIPM_H */
