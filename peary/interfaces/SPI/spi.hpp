#ifndef CARIBOU_HAL_SPI_H
#define CARIBOU_HAL_SPI_H

#include <cstdint>
#include <mutex>
#include <string>
#include <vector>

#include "interfaces/Interface.hpp"
#include "interfaces/InterfaceManager.hpp"
#include "utils/exceptions.hpp"

namespace caribou {

  using spi_t = uint8_t;
  using spi_reg_t = uint8_t;

  /* SPI command interface class
   */
  class iface_spi : public Interface<spi_reg_t, spi_t> {

  protected:
    // Default constructor: private (only created by InterfaceManager)
    // It can throw DeviceException
    explicit iface_spi(const configuration_type& config);

    ~iface_spi() override;

    // Descriptor of the device
    int spiDesc;

    // Protects access to the bus
    std::mutex mutex;

    // SPI bits per word
    const uint32_t bits_per_word = 8;

    GENERATE_FRIENDS()

    std::pair<spi_reg_t, spi_t> write(const std::pair<spi_reg_t, spi_t>& data) override;
    dataVector_type write(const spi_t& reg, const dataVector_type& data) override;
    std::vector<std::pair<spi_reg_t, spi_t>> write(const std::vector<std::pair<spi_reg_t, spi_t>>& data) override;
    dataVector_type read(const spi_reg_t& reg, const unsigned int length) override;

    // only this function can create the interface
    friend iface_spi& InterfaceManager::getInterface<iface_spi>(const configuration_type&);

    // only this function can delete the interface
    friend void InterfaceManager::deleteInterface<iface_spi>(iface_spi*);

  public:
    // Unused constructor
    iface_spi() = delete;

  }; // class iface_spi

} // namespace caribou

#endif /* CARIBOU_HAL_SPI_H */
