variables:
  GIT_SUBMODULE_STRATEGY: recursive
  EOS_PATH: "/eos/project/c/caribou/www/"

stages:
  - docker
  - compile
  - format
  - unittest
  - documentation
  - deployment
  - rebuild_meta-caribou

#######################
# Build Ubuntu image  #
#######################

docker_ubuntu:
  stage: docker
  only:
    - schedules
    - web
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  tags:
    - docker
  script:
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"auth\":\"$(echo -n ${CI_REGISTRY_USER}:${CI_REGISTRY_PASSWORD} | base64)\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --context $CI_PROJECT_DIR/etc/dockerfiles/ --dockerfile $CI_PROJECT_DIR/etc/dockerfiles/Dockerfile_ubuntu --destination $CI_REGISTRY_IMAGE/ubuntu:$CI_COMMIT_TAG

########################
# Build CentOS8 image  #
########################

docker_centos:
  stage: docker
  only:
    - schedules
    - web
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  tags:
    - docker
  script:
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --context $CI_PROJECT_DIR/etc/dockerfiles/ --dockerfile $CI_PROJECT_DIR/etc/dockerfiles/Dockerfile_centos --destination $CI_REGISTRY_IMAGE/centos:$CI_COMMIT_TAG

#######################
# Compilation targets #
#######################

# Hidden key to define the default compile job:
.compile:
    stage: compile
    tags:
        - docker
    script:
        - mkdir build
        - cd build
        - cmake -GNinja -DCMAKE_CXX_FLAGS="-Werror" -DBUILD_ALL_DEVICES=ON -DBUILD_server=ON -DCMAKE_BUILD_TYPE=RELEASE ..
        - ninja -k0
        - ninja install
    artifacts:
        paths:
            - build
            - bin
            - lib
        expire_in: 24 hour

cmp:ubuntu-gcc:
  extends: .compile
  image: ${CI_REGISTRY_IMAGE}/ubuntu:latest
  before_script:
    - export COMPILER_TYPE="gcc"

cmp:ubuntu-llvm:
  extends: .compile
  image: ${CI_REGISTRY_IMAGE}/ubuntu:latest
  before_script:
    - export CXX=/usr/bin/clang++-10

cmp:centos8-gcc-emulation:
  extends: .compile
  only:
    - tags@Caribou/peary
    - branches@Caribou/peary
  image: ${CI_REGISTRY_IMAGE}/centos:latest
  before_script:
    - export COMPILER_TYPE="gcc"
    - source .gitlab-ci.d/init_x86_64.sh
  script:
    - mkdir build
    - cd build
    - cmake -GNinja -DCMAKE_CXX_FLAGS="-Werror" -DBUILD_ALL_DEVICES=ON -DINTERFACE_EMULATION=ON -DBUILD_server=ON -DCMAKE_BUILD_TYPE=RELEASE ..
    - ninja -k0
    - ninja install

cmp:ubuntu-llvm-emulation:
  extends: .compile
  only:
    - tags@Caribou/peary
    - branches@Caribou/peary
  image: ${CI_REGISTRY_IMAGE}/ubuntu:latest
  before_script:
    - export CXX=/usr/bin/clang++-10
  script:
    - mkdir build
    - cd build
    - cmake -GNinja -DCMAKE_CXX_FLAGS="-Werror" -DBUILD_ALL_DEVICES=ON -DINTERFACE_EMULATION=ON -DBUILD_server=ON -DCMAKE_BUILD_TYPE=RELEASE ..
    - ninja -k0
    - ninja install

rebuild_meta-caribou:
    stage: rebuild_meta-caribou
    variables:
      META_CARIBOU_REF_NAME: master
    only:
      - master@Caribou/peary
    script:
      - "curl -X POST -F token=${META_CARIBOU_TOKEN} -F ref=${META_CARIBOU_REF_NAME} https://gitlab.cern.ch/api/v4/projects/13971/trigger/pipeline"

############################
# Format and Lint Checking #
############################

# Ubuntu

fmt:ubuntu-llvm-format:
  stage: format
  tags:
    - docker
  dependencies:
    - cmp:ubuntu-llvm
    - cmp:ubuntu-llvm-emulation
  image: ${CI_REGISTRY_IMAGE}/ubuntu:latest
  script:
    - cd build/
    - ninja check-format

#############################
# Documentation Compilation #
#############################

# Compile MkDocs user manual:
cmp:usermanual:
    stage: documentation
    tags:
      - docker
    only:
        - tags@Caribou/peary
        - mkdocs@Caribou/peary
        - master@Caribou/peary
    image: gitlab-registry.cern.ch/clicdp/publications/templates/custom_ci_worker:fedora-latex-latest
    dependencies: []
    script:
        - export LC_ALL=C.UTF-8 && export LANG=C.UTF-8
        - mkdir -p public/peary-manual
        - mkdocs build
        - mv site/* public/peary-manual/
    artifacts:
        paths:
          - public
        expire_in: 3 hour



##############
# Deployment #
##############

# Automatically deploy documentation to the website
# Deployment job only executed for new tag pushs, not for every commit.
deploy-documentation:
    stage: deployment
    tags:
      - docker
    # Only run for new tags:
    only:
        - tags@Caribou/peary
        - mkdocs@Caribou/peary
        - master@Caribou/peary
    dependencies:
        - cmp:usermanual
    # Docker image with tools to deploy to EOS
    image: gitlab-registry.cern.ch/ci-tools/ci-web-deployer:latest
    script:
        - deploy-eos
    # do not run any globally defined before_script or after_script for this step
    before_script: []
    after_script: []

# Deploy to meta-caribou Yocto distribution
rebuild_meta-caribou:
    stage: deployment
    variables:
      META_CARIBOU_REF_NAME: master
    only:
      - master@Caribou/peary
    script:
      - "curl -X POST -F token=${META_CARIBOU_TOKEN} -F ref=${META_CARIBOU_REF_NAME} https://gitlab.cern.ch/api/v4/projects/13971/trigger/pipeline"
